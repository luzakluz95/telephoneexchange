﻿using System;
using System.Collections.ObjectModel;
using Prism.Commands;
using Prism.Mvvm;
using TelephoneExchange.Infrastructure.Settings;
using TelephoneExchange.Main.Logic;
using TelephoneExchange.Main.Model;

namespace TelephoneExchange.Main.ViewModels
{
	public class MainScreenViewModel : BindableBase
	{
		private CallsDispatcher _callsDispatcher;
		private CallGenerator _callGenerator;
		private ConsoleOutput _consoleOutput;
		private string _agentName;
		private Agent _selectedAgent;

		public string AgentName
		{
			get => _agentName;
			set
			{
				SetProperty(ref _agentName, value);
				OnAgentNameChanged();
			}
		}

		public Agent SelectedAgent
		{
			get => _selectedAgent;
			set
			{
				SetProperty(ref _selectedAgent, value);
				OnAgentSelectionChanged();
			}
		}

		public ObservableCollection<Agent> Agents => _callsDispatcher.Agents;
		public string ConsoleLogs => _consoleOutput.OutputString;

		public DelegateCommand AddAgent { get; }
		public DelegateCommand DeleteAgent { get; }
		public DelegateCommand AddCall { get; }

		public MainScreenViewModel(ISettings settings)
		{
			_callsDispatcher = new CallsDispatcher(settings);
			_callGenerator = new CallGenerator(settings);
			_consoleOutput = new ConsoleOutput(settings.ConsoleLength);

			_consoleOutput.OutputUpdated += OnOutputUpdated;

			_callsDispatcher.AgentAdded += OnAgentAdded;
			_callsDispatcher.AgentRemoved += OnAgentRemoved;
			_callsDispatcher.CallAssigned += OnCallAssigned;
			_callsDispatcher.CallEnqueued += OnCallEnqueued;
			_callsDispatcher.CallFinished += OnCallFinished;

			_callGenerator.IncommingCall += OnIncommingCall;
			_callGenerator.StartGenerating();

			AddAgent = new DelegateCommand(AddAgentCommand, () => !string.IsNullOrWhiteSpace(AgentName));
			DeleteAgent = new DelegateCommand(DeleteAgentCommand, () => SelectedAgent != null);
			AddCall = new DelegateCommand(AddCallCommand);
		}

		private void AddAgentCommand()
		{
			_callsDispatcher.AddAgent(AgentName);
			AgentName = string.Empty;
		}

		private void DeleteAgentCommand()
		{
			_callsDispatcher.DeleteAgent(SelectedAgent);
			RaisePropertyChanged(nameof(Agents));
			SelectedAgent = null;
		}

		private void AddCallCommand()
		{
			_callsDispatcher.AddCall();
		}

		private void OnAgentNameChanged()
		{
			AddAgent.RaiseCanExecuteChanged();
		}

		private void OnAgentSelectionChanged()
		{
			DeleteAgent.RaiseCanExecuteChanged();
		}

		private void OnIncommingCall(object sender, EventArgs e)
		{
			_callsDispatcher.AddCall();
		}

		private void OnAgentAdded(object sender, string e)
		{
			_consoleOutput.PrintLine($"Agent {e} został dodany.");
		}

		private void OnAgentRemoved(object sender, string e)
		{
			_consoleOutput.PrintLine($"Agent {e} został usunięty.");
		}

		private void OnCallAssigned(object sender, Events.CallTakenEventArgs e)
		{
			_consoleOutput.PrintLine($"Rozmowa rozpoczęta przez {e.Agent.Name}.");
		}

		private void OnCallEnqueued(object sender, Events.CallEnqueuedEventArgs e)
		{
			_consoleOutput.PrintLine($"Nowe połączenie o id {e.Call.Id} zostało zakolejkowane.{Environment.NewLine}Liczba połączeń w kolejce: {e.CallsCount}");
		}

		private void OnCallFinished(object sender, Events.CallTakenEventArgs e)
		{
			_consoleOutput.PrintLine($"Rozmowa o długości {e.Call.DurationInSec}s została zakończona przez {e.Agent.Name}.");
		}

		private void OnOutputUpdated(object sender, EventArgs e)
		{
			RaisePropertyChanged(nameof(ConsoleLogs));
		}
	}
}
