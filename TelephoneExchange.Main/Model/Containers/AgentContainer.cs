﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace TelephoneExchange.Main.Model.Containers
{
	public class AgentContainer
	{
		public ObservableCollection<Agent> Agents { get; }
		public bool IsAgentNotBusy => Agents.Any(x => !x.IsBusy);

		public AgentContainer()
		{
			Agents = new ObservableCollection<Agent>();
		}

		public Agent AddAgent(string name)
		{
			var a = new Agent(Guid.NewGuid(), name);
			Agents.Add(a);
			return a;
		}

		public void DeleteAgent(Agent agent)
		{
			Agents.Remove(agent);
		}
	}
}
