﻿using System;
using System.Collections.Generic;
using System.Linq;
using TelephoneExchange.Infrastructure.Settings;

namespace TelephoneExchange.Main.Model.Containers
{
	public class CallContainer
	{
		private readonly ISettings _settings;
		private Random _randomGenerator;

		public List<Call> Calls { get; }
		public bool IsCallWainting => Calls.Any();
		public int CallsCount => Calls.Count;

		public CallContainer(ISettings settings)
		{
			_settings = settings;
			_randomGenerator = new Random();
			Calls = new List<Call>();
		}

		public Call AddRandomCall()
		{
			var c = new Call(Guid.NewGuid(), _randomGenerator.Next((int)_settings.CallDurationMinInSec, (int)_settings.CallDurationMaxInSec));
			Calls.Add(c);
			return c;
		}

		public Call TakeCall()
		{
			var c = Calls.First();
			Calls.Remove(c);
			return c;
		}
	}
}
