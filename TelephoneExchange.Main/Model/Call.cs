﻿using System;

namespace TelephoneExchange.Main.Model
{
	public class Call
	{
		public Guid Id { get; }
		public int DurationInSec { get; }

		public Call(Guid id, int durationInSec)
		{
			Id = id;
			DurationInSec = durationInSec;
		}
	}
}