﻿using System;
using System.Threading.Tasks;
using TelephoneExchange.Main.Events;

namespace TelephoneExchange.Main.Model
{
	public class Agent
	{
		public Guid Id { get; }
		public string Name { get; }
		public bool IsBusy { get; private set; }

		public event EventHandler<CallTakenEventArgs> CallFinished;

		private Call _currentCall;

		public Agent(Guid id, string name)
		{
			Id = id;
			Name = name;
			IsBusy = false;
		}

		public void ReceiveCall(Call call)
		{
			_currentCall = call;
			IsBusy = true;
			PerformCall();
		}

		private async Task PerformCall()
		{
			await Task.Delay(_currentCall.DurationInSec * 1000);
			IsBusy = false;
			CallFinished?.Invoke(this, new CallTakenEventArgs(_currentCall, this));
		}
	}
}
