﻿using Prism.Ioc;
using Prism.Modularity;
using Prism.Mvvm;
using TelephoneExchange.Infrastructure.ViewNames;

namespace TelephoneExchange.Main
{
	public class MainModule : IModule
	{
		public void OnInitialized(IContainerProvider containerProvider)
		{
		}

		public void RegisterTypes(IContainerRegistry containerRegistry)
		{
			ViewModelLocationProvider.Register<Views.MainScreen, ViewModels.MainScreenViewModel>();

			containerRegistry.RegisterForNavigation<Views.MainScreen>(ViewNames.MainScreen);
		}
	}
}
