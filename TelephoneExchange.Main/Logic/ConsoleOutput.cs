﻿using Prism.Mvvm;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

namespace TelephoneExchange.Main.Logic
{
	class ConsoleOutput
	{
		private object _syncObject = new object();
		private readonly uint _outputLength;

		public ObservableCollection<string> Output { get; }
		public string OutputString => string.Join("\n", Output.Select(x => x));

		public event EventHandler OutputUpdated;

		public ConsoleOutput(uint outputLength)
		{
			_outputLength = outputLength > 0 ? outputLength : 1000;
			Output = new ObservableCollection<string>();
		}

		public void PrintLine(string text)
		{
			lock (_syncObject)
			{
				PrintLineSTA(text);
			}
		}

		public void Clear()
		{
			lock (_syncObject)
			{
				ClearSTA();
			}
		}

		private void PrintLineSTA(string text)
		{
			Application.Current.Dispatcher.Invoke((Action)delegate
			{
				Output.Add(text);

				if (Output.Count > _outputLength)
					Output.RemoveAt(0);

				OutputUpdated?.Invoke(this, null);
			});
		}

		private void ClearSTA()
		{
			Application.Current.Dispatcher.Invoke((Action)delegate
			{
				Output.Clear();
				OutputUpdated?.Invoke(this, null);
			});
		}
	}
}
