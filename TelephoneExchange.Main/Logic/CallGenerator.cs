﻿using System;
using System.Threading;
using System.Threading.Tasks;
using TelephoneExchange.Infrastructure.Settings;

namespace TelephoneExchange.Main.Logic
{
	class CallGenerator : IDisposable
	{
		private readonly ISettings _settings;
		private Random _randomGenerator;
		private Task _generator;
		private CancellationTokenSource _cts;

		public event EventHandler IncommingCall;

		public CallGenerator(ISettings settings)
		{
			_randomGenerator = new Random();
			_settings = settings;
		}

		public void StartGenerating()
		{
			if (_generator == null)
			{
				if (_cts != null)
					_cts.Dispose();
				_cts = new CancellationTokenSource();

				var token = _cts.Token;
				_generator = Task.Run(async () =>
				{
					while (!token.IsCancellationRequested)
					{
						IncommingCall?.Invoke(this, null);
						await Task.Delay(_randomGenerator.Next((int)_settings.IncommingCallMinDelayInSec, (int)_settings.IncommingCallMaxDelayInSec) * 1000, token);
					}
				}, token);
			}
		}

		public void StopGenerating()
		{
			if (_generator != null)
			{
				_cts.Cancel();
				_generator?.Dispose();
				_generator = null;
			}
		}

		public void Dispose()
		{
			StopGenerating();
			_cts.Dispose();
		}
	}
}
