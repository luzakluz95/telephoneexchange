﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TelephoneExchange.Infrastructure.Settings;
using TelephoneExchange.Main.Events;
using TelephoneExchange.Main.Model;
using TelephoneExchange.Main.Model.Containers;

namespace TelephoneExchange.Main.Logic
{
	class CallsDispatcher
	{
		private AgentContainer _agentContainer;
		private CallContainer _callContainer;
		private Mutex _mutex;

		public ObservableCollection<Agent> Agents => _agentContainer.Agents;

		public event EventHandler<CallEnqueuedEventArgs> CallEnqueued;
		public event EventHandler<CallTakenEventArgs> CallAssigned;
		public event EventHandler<CallTakenEventArgs> CallFinished;
		public event EventHandler<string> AgentAdded;
		public event EventHandler<string> AgentRemoved;

		#region ctor
		public CallsDispatcher(ISettings settings)
		{
			_agentContainer = new AgentContainer();
			_callContainer = new CallContainer(settings);
			_mutex = new Mutex();
		}
		#endregion

		#region Agent manipulation
		public void AddAgent(string name)
		{
			var newAgent = _agentContainer.AddAgent(name);
			newAgent.CallFinished += OnCallFinished;
			AgentAdded?.Invoke(this, name);
			OnAgentAdded();
		}

		public void DeleteAgent(Agent agent)
		{
			_agentContainer.DeleteAgent(agent);
			AgentRemoved?.Invoke(this, agent.Name);
		}
		#endregion

		#region Call manipulation
		public void AddCall()
		{
			var newCall = _callContainer.AddRandomCall();
			CallEnqueued?.Invoke(this, new CallEnqueuedEventArgs(newCall, _callContainer.CallsCount));
			OnCallAdded();
		}
		#endregion

		private void OnCallFinished(object sender, CallTakenEventArgs e)
		{
			CallFinished?.Invoke(this, e);
			AssignCall();
		}

		private async Task OnCallAdded()
		{
			await Task.Run(() => AssignCall());
		}

		private async Task OnAgentAdded()
		{
			await Task.Run(() => AssignCall());
		}

		private void AssignCall()
		{
			_mutex.WaitOne();
			if (_callContainer.IsCallWainting && _agentContainer.IsAgentNotBusy)
			{
				var c = _callContainer.TakeCall();
				var a = Agents.First(x => !x.IsBusy);
				a.ReceiveCall(c);
				CallAssigned?.Invoke(this, new CallTakenEventArgs(c, a));
			}
			_mutex.ReleaseMutex();
		}
	}
}
