﻿using TelephoneExchange.Main.Model;

namespace TelephoneExchange.Main.Events
{
	public class CallTakenEventArgs
	{
		public Call Call { get; }
		public Agent Agent { get; }

		public CallTakenEventArgs(Call call, Agent agent)
		{
			Call = call;
			Agent = agent;
		}
	}
}
