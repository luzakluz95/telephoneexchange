﻿using TelephoneExchange.Main.Model;

namespace TelephoneExchange.Main.Events
{
	public class CallEnqueuedEventArgs
	{
		public Call Call { get; }
		public int CallsCount { get; }

		public CallEnqueuedEventArgs(Call call, int callsCount)
		{
			Call = call;
			CallsCount = callsCount;
		}
	}
}
