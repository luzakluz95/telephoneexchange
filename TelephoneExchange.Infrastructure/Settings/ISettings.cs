﻿namespace TelephoneExchange.Infrastructure.Settings
{
	public interface ISettings
	{
		uint ConsoleLength { get; }
		uint IncommingCallMinDelayInSec { get; }
		uint IncommingCallMaxDelayInSec { get; }
		uint CallDurationMinInSec { get; }
		uint CallDurationMaxInSec { get; }
	}
}
