﻿namespace TelephoneExchange.Infrastructure.Settings
{
	public class Settings : ISettings
	{
		public uint ConsoleLength { get; set; }
		public uint IncommingCallMinDelayInSec { get; set; }
		public uint IncommingCallMaxDelayInSec { get; set; }
		public uint CallDurationMinInSec { get; set; }
		public uint CallDurationMaxInSec { get; set; }
	}
}
