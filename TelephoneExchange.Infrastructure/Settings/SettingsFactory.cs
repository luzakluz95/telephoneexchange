﻿using System;
using System.IO;

namespace TelephoneExchange.Infrastructure.Settings
{
	public class SettingsFactory
	{
		private const string SettingsFileName = "settings.json";

		public static ISettings InitializeSettings()
		{
			string _settingsFilePath = Path.Combine(Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory), SettingsFileName);

			if (!File.Exists(_settingsFilePath))
			{
				throw new FileNotFoundException($"Settings file {_settingsFilePath} does not exist");
			}
			return Newtonsoft.Json.JsonConvert.DeserializeObject<Settings>(File.ReadAllText(_settingsFilePath));
		}
	}
}
