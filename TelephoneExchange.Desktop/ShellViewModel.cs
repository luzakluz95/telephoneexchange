﻿using Prism.Mvvm;
using Prism.Regions;
using TelephoneExchange.Infrastructure.ViewNames;

namespace TelephoneExchange.Desktop
{
	public class ShellViewModel : BindableBase
	{
		private readonly IRegionManager _regionManager;

		public ShellViewModel(IRegionManager regionManager)
		{
			_regionManager = regionManager;
		}

		public void OnLoaded()
		{
			_regionManager.RequestNavigate(RegionNames.ShellRegion, ViewNames.MainScreen);
		}
	}
}