﻿using Prism.Ioc;
using Prism.Modularity;
using TelephoneExchange.Infrastructure.Settings;

namespace TelephoneExchange.Desktop
{
	class ConfigurationFactory
	{
		internal static void RegisterTypes(IContainerRegistry containerRegistry)
		{
			containerRegistry.RegisterInstance<ISettings>(SettingsFactory.InitializeSettings());
		}

		internal static void ConfigureModuleCatalog(IModuleCatalog moduleCatalog)
		{
			moduleCatalog.AddModule<TelephoneExchange.Main.MainModule>();
		}
	}
}
