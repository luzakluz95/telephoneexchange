﻿using Prism.Ioc;
using Prism.Modularity;
using Prism.Unity;
using System.Windows;
using TelephoneExchange.Desktop;

namespace TelephoneExchange.Desktop
{
	public partial class App : PrismApplication
	{
		protected override void RegisterTypes(IContainerRegistry containerRegistry)
		{
			ConfigurationFactory.RegisterTypes(containerRegistry);
		}

		protected override void ConfigureModuleCatalog(IModuleCatalog moduleCatalog)
		{
			ConfigurationFactory.ConfigureModuleCatalog(moduleCatalog);
		}

		protected override Window CreateShell()
		{
			var window = Container.Resolve<Shell>();
			return window;
		}
	}
}
