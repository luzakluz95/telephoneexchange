﻿using System.Windows;

namespace TelephoneExchange.Desktop
{
	public partial class Shell : Window
	{
		public Shell(ShellViewModel viewModel)
		{
			DataContext = viewModel;
			InitializeComponent();

			Loaded += Shell_Loaded;
		}

		private void Shell_Loaded(object sender, RoutedEventArgs e)
		{
			(DataContext as ShellViewModel).OnLoaded();
		}
	}
}
